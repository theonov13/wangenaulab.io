# [wangenau.gitlab.io](https://wangenau.gitlab.io)
[![license](https://img.shields.io/badge/license-MIT-green)](LICENSE)
[![html](https://img.shields.io/badge/language-HTML5-orange)](https://www.w3.org/html)
[![css](https://img.shields.io/badge/language-CSS3-blue)](https://www.w3.org/Style/CSS)
[![js](https://img.shields.io/badge/language-JS-yellow)](https://www.ecma-international.org/technical-committees/tc39)

This is the source code for my homepage.

The following ideas were pursued while building the webpage:

* Static HTML5 webpage
* Clean and modern UI
* Light and dark mode
* Responsive design
* Functional under modern browsers
* Functional with JavaScript disabled
* Accessible

## Resources

The following links provide awesome articles and resources that helped a lot while building the webpage
* Colors
  * https://material.io/archive/guidelines/style/color.html
  * https://codepen.io/sosuke/pen/Pjoqqp
* Icons
  * https://materialdesignicons.com
* Dark mode
  * https://css-tricks.com/a-complete-guide-to-dark-mode-on-the-web
* Table of contents
  * https://css-tricks.com/sticky-table-of-contents-with-scrolling-active-states
* Collapsible
  * https://www.digitalocean.com/community/tutorials/css-collapsible
* CSS reset
  * https://github.com/jgthms/minireset.css
* Minify
  * https://github.com/tdewolff/minify
* Linter and Validator
  * https://validator.w3.org
  * https://www.browseemall.com/Compatibility/ValidateHTML
  * https://jigsaw.w3.org/css-validator
  * https://www.browseemall.com/Compatibility/ValidateCSS
  * https://www.cssportal.com/css-optimize
  * https://www.jslint.com

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Issues

If you run into issues while using the webpage the [creation of an issue](https://gitlab.com/wangenau/wangenau.gitlab.io/-/issues) is highly appreciated.

Please note that very old browsers like Internet Explorer were not considered while testing.

## Storytime

This is the second attempt at building this site. The first version was almost done when suddenly my hard drive died. This retaught me two things:

1. Never forget to push your changes
2. Always hate web design
