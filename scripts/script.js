// Set theme toggle to visible if javascript is enabled
const toggle = document.getElementById("theme_toggle");
toggle.style.display = "block";

// Set theme on page opening if a setting exists
const currentTheme = localStorage.getItem("theme");
if (currentTheme === "dark" || currentTheme === "light") {
  document.documentElement.classList.add(currentTheme);
  document.getElementById("theme_toggle").src = "images/" + currentTheme + ".svg";
}

// Theme toggle functionality
function toggleDarkMode() {
  let theme;
  if (document.documentElement.classList.contains("light")) {
    document.documentElement.classList.remove("light");
    theme = "dark";
  } else if (document.documentElement.classList.contains("dark")) {
    document.documentElement.classList.remove("dark");
    theme = "light";
  } else {
    theme = (
      window?.matchMedia("(prefers-color-scheme: dark)").matches ? "light" : "dark"
    );
  }
  document.documentElement.classList.add(theme);
  document.getElementById("theme_toggle").src = "images/" + theme + ".svg";
  localStorage.setItem("theme", theme);
}

// Scrolling navigation bar
window.addEventListener("DOMContentLoaded", () => {
  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      const id = entry.target.getAttribute("id");
      if (entry.intersectionRatio > 0) {
        document.querySelector(`nav li a[href="#${id}"]`).parentElement.classList.add("active");
      } else {
        document.querySelector(`nav li a[href="#${id}"]`).parentElement.classList.remove("active");
      }
    });
  });
  document.querySelectorAll("section[id]").forEach((section) => {
    observer.observe(section);
  });
});

// Accessible theme toggle and collapsible
const myLabels = document.querySelectorAll(".theme, .toggle-label");
Array.from(myLabels).forEach((label) => {
  label.addEventListener("keydown", (event) => {
    if (event.key === "Enter" || event.key === " ") {
      event.preventDefault();
      label.click();
    }
  });
});
